abstract class Handle{
  void  process<T>(T);

  String get capture;
}

class HandleControl{
  static final _list = <Handle>[];

  static void process<T>(String str, T data){
    Handle  handle =  _list.singleWhere((it) => it.capture == str,orElse: () => null);
    if(handle != null){
      handle.process(data);
    }
  }

  void add(Handle h){
    _list.add(h);
  }

  void del(String str){
    _list.removeWhere((t) => t.capture == str);
  }
}
