import 'package:flutter/material.dart';
import 'package:flutter_app/eventbus.dart';

void main() {
  runApp(MyApp());
}


class MyApp extends StatelessWidget  {

  static final EventBus eventBus = EventBus();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
//    EventChannel
    return MaterialApp(
      title: 'Flutter Demo',
      home: Scaffold(body: Center(child: Container(child:MyStatefulWidget()),
      )),
    );
  }
}


class MyStatefulWidget extends StatefulWidget{

  @override
  State createState() {
    return MyStatefulWidgetState();
  }
}

class MyStatefulWidgetState extends State<MyStatefulWidget>{

  int tick = 0;

  @override
  Widget build(BuildContext context) {
    return RaisedButton(child: Text(
        tick.toString(),
        style: TextStyle(color: Colors.white)),
      color: Colors.blue,
      onPressed: (){
        setState((){
           MyApp.eventBus.topic<int>("button.click").receive((t){
             tick = t;
             print(tick);
           });
         MyApp.eventBus.topic<int>("button.click").send(tick);
        });
      },);
  }
}