package com.example.flutterapp;

import android.os.Bundle;
import io.flutter.app.FlutterActivity;
import io.flutter.eventbus.EventBus;
import io.flutter.eventbus.Topic;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class MainActivity extends FlutterActivity {
  private EventBus eventBus;
  private Topic<Integer> topic;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    GeneratedPluginRegistrant.registerWith(this);
    eventBus = new EventBus(this.getFlutterView());
    topic = eventBus.topic("button.click");
    topic.receive(integer -> topic.send(integer+1));
  }
}