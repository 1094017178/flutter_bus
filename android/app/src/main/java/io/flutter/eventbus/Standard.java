package io.flutter.eventbus;

public class Standard {
    final  static String argumentName = "data";
    final  static String receive = "receive";
    final  static String topic = "topic";
    final  static String success = "ok";
    final  static String failure = "fail";
}
