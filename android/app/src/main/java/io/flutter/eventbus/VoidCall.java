package io.flutter.eventbus;

@FunctionalInterface
public interface VoidCall<T>{
    void call(T t);
}